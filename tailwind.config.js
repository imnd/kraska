const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: ["./src/index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false,
  plugins: [require("@tailwindcss/typography"), require("@tailwindcss/forms")],
  theme: {
    fontFamily: {
      sans: ["Fugue", ...defaultTheme.fontFamily.sans],
      formular: ["Formular"],
      rodchenko: ["Rodchenko"],
      "rodchenko-light": ["Rodchenko-Light"],
      "rodchenko-regular": ["Rodchenko-Regular"],
      "rodchenko-cond-light": ["RodchenkoCond-Light"],
      "rodchenko-cond-regular": ["RodchenkoCond-Regular"],
    },
    extend: {
      fontSize: {
        xxxs: "0.5625rem",
        xxs: "0.625rem",
        "1.5xl": "1.375rem",
        "4.5xl": "2.5rem",
      },
      width: {
        5.25: "1.3125rem",
        34.5: "8.625rem",
        68: "17rem",
        95.5: "23.875rem",
        98: "24.5rem",
        112.5: "28.125rem",
        125: "31.25rem",
        135: "33.75rem",
        145: "36.25rem",
        190: "47.5rem",
        194: "48.5rem",
        200: "50rem",
      },
      minWidth: {
        100: "25rem",
        124: "31rem",
      },
      height: {
        7.5: "1.875rem",
        62.5: "15.625rem",
        90: "22.5rem",
      },
      maxHeight: {
        mmenu: "calc(100vh - 2rem)",
        initial: "initial",
      },
      margin: {
        18: "4.5rem",
      },
      padding: {
        0.8: "0.2rem",
        5.5: "1.375rem",
      },
    },
  },
};
