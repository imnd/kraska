import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import eslint from "vite-plugin-eslint";
import viteSvgIcons from "vite-plugin-svg-icons";
import { viteCommonjs, esbuildCommonjs } from '@originjs/vite-plugin-commonjs'
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    watch: {
      usePolling: false,
    },
    fs: { strict: false },
  },
  plugins: [
    vue(),
    eslint({ cache: false }),
    viteSvgIcons({
      iconDirs: [path.resolve(process.cwd(), "./src/assets/svg/")],
      symbolId: "icon-[dir]-[name]",
    }),
    viteCommonjs(),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
      process: "process/browser",
      stream: "stream-browserify",
      zlib: "browserify-zlib",
      util: "util",
      web3: path.resolve(__dirname, "./node_modules/web3/dist/web3.min.js"),
    },
  },
  build: {
    commonjsOptions: {
      transformMixedEsModules: true,
    },
  },
});
