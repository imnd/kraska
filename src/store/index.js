import { createStore } from "vuex";

export default createStore({
  state: {
    jsonld: {
      "@context": "https://schema.org",
      "@type": "PaymentService",
    },
    breadcrumbs: [
      {
        title: "step number",
        link: "/step-1",
      },
      {
        title: "block title",
        link: "/block-title",
      },
    ],
    menuItems: [
      {
        title: "collections",
        siblings: [
          {
            title: "2021 winter",
            link: "/collections/winter-2021",
          },
          {
            title: "2022 summer",
            link: "/collections/summer-2022",
          },
          {
            title: "2023 spring",
            link: "/collections/winter-2023",
          },
        ],
      },
      {
        title: "about",
        link: "/about",
      },
      {
        title: "account",
        link: "/account",
      },
      {
        title: "bag",
        link: "/bag",
      },
      {
        title: "shipping",
        link: "/shipping",
      },
      {
        title: "contacts",
        link: "/contacts",
      },
      {
        title: "redeem nft",
        siblings: [
          {
            title: "redemption checker",
            link: "/redeem/redemption-checker",
          },
          {
            title: "redeem",
            link: "/redeem",
          },
        ],
      },
      {
        title: "faq",
        link: "/faq",
      },
      {
        title: "legal",
        link: "/legal",
      },
      {
        title: "instagram",
        link: "https://www.instagram.com",
      },
      {
        title: "facebook",
        link: "https://www.facebook.com",
      },
      {
        title: "twitter",
        link: "https://twitter.com",
      },
      {
        title: "discord",
        link: "https://discord.com",
      },
    ],
    countries: [
      {
        value: "bel",
        text: "BELARUS",
      },
      {
        value: "can",
        text: "CANADA",
      },
      {
        value: "geo",
        text: "GEORGIA",
      },
      {
        value: "ger",
        text: "GERMANY",
      },
      {
        value: "gre",
        text: "GREECE",
      },
      {
        value: "eng",
        text: "ENGLAND",
      },
      {
        value: "irl",
        text: "IRELAND",
      },
      {
        value: "itl",
        text: "ITALY",
      },
      {
        value: "krk",
        text: "KROKOZHIJA",
      },
      {
        value: "rus",
        text: "RUSSIA",
      },
      {
        value: "spn",
        text: "SPAIN",
      },
      {
        value: "ukr",
        text: "UKRAINE",
      },
      {
        value: "usa",
        text: "USA",
      },
    ],
    currencies: ["usd", "rub", "eur"],
    currencySigns: {
      usd: "$",
      rub: "₽",
      eur: "€",
    },
  },
});
