const jsonld = {
  methods: {
    mergeJsonLd() {
      this.$store.state.jsonld = Object.assign(this.$store.state.jsonld, {
        name: this.title,
        description: this.description || "",
      });
    },
  },
};
export default jsonld;
