const validation = {
  data: () => ({
    filled: {},
    errors: {},
    errorSummary: "",
  }),
  methods: {
    updateForm(key, val) {
      this[key] = val;
      this.validateForm(this);
    },
    validateForm(data) {
      this.v$.$touch();
      this.errors = {};
      if (this.v$.$invalid) {
        this.v$.$errors.forEach(($error) => {
          if (data[$error.$property] !== undefined) {
            this.errors[$error.$property] = $error.$message;
          } else {
            this.errors[$error.$property] = null;
          }
        });
      }
    },
    checkForm() {
      this.v$.$touch();
      if (this.v$.$invalid) {
        this.errorSummary = "fill in the form to proceed";
        return false;
      }
      return true;
    },
  },
  computed: {
    formInvalid() {
      this.v$.$touch();
      return this.v$.$invalid;
    },
  },
};
export default validation;
