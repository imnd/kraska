const controls = {
  emits: ["click"],
  props: {
    title: {
      type: String,
      default: "",
    },
    bgColor: {
      type: String,
      default: "",
    },
    border: {
      type: String,
      default: "",
    },
    width: {
      type: String,
      default: "",
    },
    height: {
      type: String,
      default: "h-7.5 sm:h-12",
    },
    padding: {
      type: Number,
      default: 0,
    },
    class: {
      type: String,
      default: "",
    },
  },
  data: () => ({
    buttonActive: false,
    buttonHover: false,
    buttonFocus: false,
  }),
  methods: {
    focus() {
      this.buttonFocus = true;
    },
    blur() {
      this.buttonFocus = false;
    },
    mouseover() {
      this.buttonHover = true;
    },
    mouseleave() {
      this.buttonHover = false;
    },
    click(e) {
      this.buttonHover = false;
      this.buttonFocus = false;
      this.buttonActive = true;
      this.$emit("click");
    },
    getTextClass() {
      return this.textColor;
    },
    getBgClass() {
      return this.bgColor;
    },
    getBorderClass() {
      return this.borderColor;
    },
  },
  computed: {
    cssClass() {
      return (
        " text-xxs sm:text-xs " +
        this.class +
        this.colorClasses +
        " border" +
        (this.border ? "-" + this.border : "") +
        " px-" +
        this.padding +
        " " +
        this.widthClass +
        " " +
        this.height
      );
    },
    widthClass() {
      return this.width;
    },
    colorClasses() {
      return (
        " " +
        this.getBgClass() +
        " " +
        this.getTextClass() +
        " " +
        this.getBorderClass()
      );
    },
  },
};
export default controls;
