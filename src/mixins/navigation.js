const navigation = {
  methods: {
    back() {
      this.$router.go(-1);
    },
  },
};

export default navigation;
