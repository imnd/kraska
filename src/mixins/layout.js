const layout = {
  computed: {
    justify() {
      if (this.$route.meta.justify === undefined) {
        return "justify-start";
      }
      return this.$route.meta.justify;
    },
  },
};
export default layout;
