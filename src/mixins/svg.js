const svg = {
  props: {
    class: {
      type: String,
      default: "",
    },
    width: {
      type: Number,
      default: 10,
    },
    height: {
      type: Number,
      default: 10,
    },
    rotate: {
      type: Number,
      default: 0,
    },
    scale: {
      type: Number,
      default: 1,
    },
  },
  computed: {
    cssClass() {
      return this.class;
    },
    localWidth() {
      return Math.abs(this.rotate) === 90 ? this.height : this.width;
    },
    localHeight() {
      return Math.abs(this.rotate) === 90 ? this.width : this.height;
    },
  },
};
export default svg;
