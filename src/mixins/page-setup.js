import jsonld from "@/mixins/jsonld.js";

const pageSetup = {
  mixins: [jsonld],
  created() {
    this.mergeJsonLd();
  },
};

export default pageSetup;
