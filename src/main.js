import { createApp } from "vue";
import App from "./App.vue";
import "./index.css";

import { Buffer } from "buffer";
window.Buffer = Buffer;

import process from "process";
window.process = process;

import EventEmitter from "events";
window.EventEmitter = EventEmitter;

import store from "./store";
import router from "./router";
createApp(App).use(store).use(router).mount("#app");
