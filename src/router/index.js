import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "home",
    meta: {
      animation: true,
      justify: "justify-center",
    },
    component: () => import("../pages/home.vue"),
  },
  {
    path: "/popup",
    name: "popup",
    meta: {
      animation: true,
      justify: "justify-center",
    },
    component: () => import("../pages/popup.vue"),
  },
  {
    path: "/auth",
    name: "auth",
    component: () => import("../pages/auth.vue"),
  },
  {
    path: "/user-profile/bag",
    name: "bag",
    meta: {
      layout: "full-screen",
      justify: "justify-center sm:justify-start",
    },
    component: () => import("../pages/user-profile/bag/bag.vue"),
  },
  {
    path: "/user-profile/account",
    name: "user-account",
    component: () => import("../pages/user-profile/account.vue"),
  },
  {
    path: "/user-profile/orders",
    name: "user-orders",
    component: () => import("../pages/user-profile/orders.vue"),
  },
  {
    path: "/user-profile/delivery",
    name: "user-delivery",
    component: () => import("../pages/user-profile/delivery.vue"),
  },
  {
    path: "/collections/:period",
    name: "collections",
    component: () => import("../pages/collection.vue"),
  },
  {
    path: "/redeem",
    name: "redeem",
    meta: {
      justify: "justify-center",
    },
    component: () => import("../pages/redeem/redeem.vue"),
  },
  {
    path: "/redeem/redemption-checker",
    name: "redemption-checker",
    meta: {
      justify: "justify-start md:justify-center",
    },
    component: () => import("../pages/redeem/redemption-checker.vue"),
  },
  {
    path: "/redeem/card-item/:id",
    name: "card-item",
    meta: {
      layout: "full-screen",
      justify: "justify-start md:justify-center",
    },
    component: () => import("../pages/redeem/card-item.vue"),
  },
  {
    path: "/wallet",
    name: "wallet",
    meta: {
      justify: "justify-start md:justify-center",
    },
    component: () => import("../pages/redeem/wallet.vue"),
  },
  {
    path: "/faq",
    name: "faq",
    component: () => import("../pages/faq.vue"),
  },
  {
    path: "/about",
    name: "about",
    component: () => import("../pages/about.vue"),
  },
  {
    path: "/contacts",
    name: "contacts",
    meta: { justify: "justify-center" },
    component: () => import("../pages/contacts.vue"),
  },
  {
    path: "/privacy-policy",
    name: "privacy-policy",
    component: () => import("../pages/privacy-policy.vue"),
  },
  {
    path: "/service-terms",
    name: "service-terms",
    component: () => import("../pages/service-terms.vue"),
  },
  {
    path: "/error/:code",
    name: "error",
    meta: {
      animation: true,
      justify: "justify-start md:justify-center",
    },
    component: () => import("../pages/error.vue"),
  },
  {
    path: "/:catchAll(.*)",
    redirect: "/error/404",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
